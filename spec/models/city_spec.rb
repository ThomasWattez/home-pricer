# frozen_string_literal: true

require 'rails_helper'

RSpec.describe City, type: :model do
  describe 'fetch_transactions!' do
    subject { city.fetch_transactions! }

    let(:city) { create(:city) }
    let(:url) do
      'https://data.opendatasoft.com/api/records/1.0/search/?' \
        'dataset=demande-de-valeurs-foncieres-agrege-a-la-transaction%40public&q=' \
        "&facet=code_postal&refine.code_postal=#{city.postal_code}"
    end

    before do
      fixture = File.read(Rails.root.join('spec/fixtures/api_response.json'))
      allow(Typhoeus).to receive(:get).with(url).and_return(double(:resp, response_body: fixture))
    end

    it 'fetches the transactions from the API and attaches them to the city' do
      expect { subject }.to(change { city.transactions.count }.by(10))
    end

    context 'when a transaction was already synced' do
      let!(:trx) do
        create(:transaction, city: city, number_of_rooms: 3, record_id: '8a310c93b12c274d7d2fa1aabfb8b5e949337c91')
      end

      it 'updates the transactions fields' do
        subject
        expect(trx.reload.number_of_rooms).to eq(1)
      end
    end
  end
end
