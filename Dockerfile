# https://hub.docker.com/_/ruby
FROM ruby:3.0.5

# Install bundler
RUN gem update --system 3.2.33
RUN gem install bundler

# Directory within container
WORKDIR /usr/src/app

# Install production dependencies.
COPY Gemfile Gemfile.lock ./
# ENV BUNDLE_FROZEN=true # --> Will prevent Gemfile.lock to be reconstructed at build time
RUN bundle install

# Install node & Percy CLI
RUN apt-get update -yq && apt-get upgrade -yq && apt-get install -yq curl wget
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash
RUN apt-get install nodejs
RUN npm install -g @percy/cli

# Install Chrome (required for Percy)
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get -y install ./google-chrome-stable_current_amd64.deb

# Copy local code to the container image.
COPY . ./

# Expose port 3000
EXPOSE 3000

# Run the web service on container startup.
CMD ["bash", "entrypoint.sh"]